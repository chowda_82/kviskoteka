package ivancordasic.ferit.kviskoteka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ivancordasic.ferit.kviskoteka.QuizContract.*;

public class QuizDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Kviskoteka.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

    private static QuizDbHelper instance;

    private  QuizDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized QuizDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new QuizDbHelper(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        final String SQL_CREATE_CATEGORIES_TABLE = "CREATE TABLE " +
                CategoriesTable.TABLE_NAME + "( " +
                CategoriesTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CategoriesTable.COLUMN_NAME + " TEXT " +
                ")";

        final String SQL_CREATE_QUESTIONS_TABLE = "CREATE TABLE " +
                QuestionsTable.TABLE_NAME + " ( " +
                QuestionsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuestionsTable.COLUMN_QUESTION + " TEXT, " +
                QuestionsTable.COLUMN_OPTION1 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION2 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION3 + " TEXT, " +
                QuestionsTable.COLUMN_ANSWER_NR + " INTEGER, " +
                QuestionsTable.COLUMN_DIFFICULTY + " TEXT, " +
                QuestionsTable.COLUMN_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY(" + QuestionsTable.COLUMN_CATEGORY_ID + ") REFERENCES " +
                CategoriesTable.TABLE_NAME + "(" + CategoriesTable._ID + ")" + "ON DELETE CASCADE" +
                ")";

        db.execSQL(SQL_CREATE_CATEGORIES_TABLE);
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        fillCategoriesTable();
        fillQuestionsTable();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CategoriesTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuestionsTable.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private void fillCategoriesTable() {
        Category c1 = new Category("Opće znanje");
        addCategory(c1);
        Category c2 = new Category("Znanost");
        addCategory(c2);
        Category c3 = new Category("Geografija");
        addCategory(c3);
    }

    private void addCategory(Category category) {
        ContentValues cv = new ContentValues();
        cv.put(CategoriesTable.COLUMN_NAME, category.getName());
        db.insert(CategoriesTable.TABLE_NAME, null, cv);
    }

    private void fillQuestionsTable() {
        Question q1 = new Question("Koje je godine Titanik potonuo u Atlantski ocean?",
                "1912.", "1911.", "2020.", 1,
                Question.DIFFICULTY_EASY, Category.OPCEZNANJE);
        addQuestion(q1);
        Question q2 = new Question("Koliko je predsjedsnika do sada imala RH?",
                "2", "5", "1", 2,
                Question.DIFFICULTY_EASY, Category.OPCEZNANJE);
        addQuestion(q2);
        Question q3 = new Question("Koji je glavni grad Hrvatske?",
                "Vinkovci", "Split", "Zagreb", 3,
                Question.DIFFICULTY_EASY, Category.OPCEZNANJE);
        addQuestion(q3);
        Question q4 = new Question("Koji se grad naziva Vrata Hrvatske?",
                "Vinkovci", "Osijek", "Vukovar", 1,
                Question.DIFFICULTY_EASY, Category.OPCEZNANJE);
        addQuestion(q4);
        Question q5 = new Question("Najpoznatiji hrvatski nogometaš?",
                "Luka Modrić", "Boško Balaban", "Niko Kranjčar", 1,
                Question.DIFFICULTY_EASY, Category.OPCEZNANJE);
        addQuestion(q5);
        Question q6 = new Question("Kako se zove najveća tehnološka kompanija u Južnoj Koreji?",
                "Google.", "Xiaomi", "Samsung", 3,
                Question.DIFFICULTY_MEDIUM, Category.OPCEZNANJE);
        addQuestion(q6);
        Question q7 = new Question("Koje je puno ime lutke Barbie?",
                "Gotz Kati", "Baby Gluck", "Barbara Millicent Roberts", 3,
                Question.DIFFICULTY_MEDIUM, Category.OPCEZNANJE);
        addQuestion(q7);
        Question q8 = new Question("Tko je izumio hranu za konzerviranje?",
                "Nikola Tesla", "Ivo Andrić", " Peter Durand", 3,
                Question.DIFFICULTY_MEDIUM, Category.OPCEZNANJE);
        addQuestion(q8);
        Question q9 = new Question("Tko je od 1950. do 1967. 15 puta osvojio Davis Cup?",
                "Velika Britanija", "Australija", "Francuska", 2,
                Question.DIFFICULTY_MEDIUM, Category.OPCEZNANJE);
        addQuestion(q9);
        Question q10 = new Question("Kad je započeo Domovinski rat?",
                "1991.", "1992.", "1993.", 1,
                Question.DIFFICULTY_MEDIUM, Category.OPCEZNANJE);
        addQuestion(q10);
        Question q11 = new Question(" Koji je metal otkrio Hans Christian Oersted 1825. godine?",
                "Aluminij", "Srebro", "Cink", 1,
                Question.DIFFICULTY_HARD, Category.OPCEZNANJE);
        addQuestion(q11);
        Question q12 = new Question("Tko je bio premijer Velike Britanije od 1841. do 1846. godine?",
                "Boris Johnson", "Robert Peel", "Theresa May", 2,
                Question.DIFFICULTY_HARD, Category.OPCEZNANJE);
        addQuestion(q12);
        Question q13 = new Question("Tko je izumio mačje oči 1934. da bi poboljšao sigurnost na cestama?",
                "Nikola Tesla", "Ivo Andrić", " Peter Durand", 3,
                Question.DIFFICULTY_HARD, Category.OPCEZNANJE);
        addQuestion(q13);
        Question q14 = new Question("Tko je od 1950. do 1967. 15 puta osvojio Davis Cup?",
                "Percy Shaw", "Thomas Alva Edison", "Alexander Graham Bell", 1,
                Question.DIFFICULTY_HARD, Category.OPCEZNANJE);
        addQuestion(q14);
        Question q15 = new Question("Što je u posjetnici Al Caponea navedeno njegovo zanimanje?",
                "Prodavač rabljenog namještaja", "Noćni bravar", "Mafijaš", 1,
                Question.DIFFICULTY_HARD, Category.OPCEZNANJE);
        addQuestion(q15);



        Question q16 = new Question("Heksadekadski broj A2 je dekadski?",
                "163", "162", "185", 2,
                Question.DIFFICULTY_EASY, Category.ZNANOST);
        addQuestion(q16);
        Question q17 = new Question("Kojom se tipkom na tipkovnici uobičajeno pokreće sustav pomoći?",
                "F1", "F2", "F3", 1,
                Question.DIFFICULTY_EASY, Category.ZNANOST);
        addQuestion(q17);
        Question q18 = new Question("Koji je dio mozga odgovoran za kontrolu motorike?",
                "Mali mozak", "Veliki mozak", "Hipokampus", 1,
                Question.DIFFICULTY_EASY, Category.ZNANOST);
        addQuestion(q18);
        Question q19 = new Question("Koliko približno iznosi broj pi?",
                "3.13", "3.15", "3.14", 3,
                Question.DIFFICULTY_EASY, Category.ZNANOST);
        addQuestion(q19);
        Question q20 = new Question("Kako se računa opseg kružnice?",
                "4*r", "2*r*pi", "pi*e", 2,
                Question.DIFFICULTY_EASY, Category.ZNANOST);
        addQuestion(q20);
        Question q21 = new Question("Grana kemije koja se bavi proučavanjem prostorne organizacije molekula je?",
                "Stereokemija.", "Stektroskopija", "Elektrokemija", 1,
                Question.DIFFICULTY_MEDIUM, Category.ZNANOST);
        addQuestion(q21);
        Question q22 = new Question("Gdje je živio Australopitek?",
                "Australija", "Europa", "Afrika", 3,
                Question.DIFFICULTY_MEDIUM, Category.ZNANOST);
        addQuestion(q22);
        Question q23 = new Question("Koja je ljudska najveća arterija?",
                "Aorta", "Plućna arterija", "Arteriola", 1,
                Question.DIFFICULTY_MEDIUM, Category.ZNANOST);
        addQuestion(q23);
        Question q24 = new Question("Ekstenzija zip ukazuje da se u datoteci nalazi?",
                "Video", "Arhiva", "Tekst", 2,
                Question.DIFFICULTY_MEDIUM, Category.ZNANOST);
        addQuestion(q24);
        Question q25 = new Question("Koliko iznosi jedna pinta?",
                "358 ml", "628 ml", "458 ml", 3,
                Question.DIFFICULTY_MEDIUM, Category.ZNANOST);
        addQuestion(q25);
        Question q26 = new Question("Hardverska komponenta za pohranu podataka na računalo?",
                "Procesor", "Tvrdi disk", "Matična ploča", 2,
                Question.DIFFICULTY_HARD, Category.ZNANOST);
        addQuestion(q26);
        Question q27 = new Question("Ekstenzija pptx ukazuje da se u datoteci nalazi?",
                "Video", "Slika", "Prezentacija", 3,
                Question.DIFFICULTY_HARD, Category.ZNANOST);
        addQuestion(q27);
        Question q28 = new Question("Gdje se bilježe najviše temperature u Sunčevom sustavu?",
                "Zemlja", "Mars", "Venera", 3,
                Question.DIFFICULTY_HARD, Category.ZNANOST);
        addQuestion(q28);
        Question q29 = new Question("Strah od psa ili?",
                "Kinofobija", "Tarofobija", "Panofobija", 1,
                Question.DIFFICULTY_HARD, Category.ZNANOST);
        addQuestion(q29);
        Question q30 = new Question("Najveći saletit u Sunčevom sustavu je?",
                "Ganymede", "Mjesec", "Titan", 1,
                Question.DIFFICULTY_HARD, Category.ZNANOST);
        addQuestion(q30);




        Question q31 = new Question("Koji je glavni grad Slovenije?",
                "Maribor", "Celje", "Ljubljana", 3,
                Question.DIFFICULTY_EASY, Category.GEOGRAFIJA);
        addQuestion(q31);
        Question q32 = new Question("Koji je glavni grad Bih?",
                "Sarajevo", "Mostar", "Jajce", 1,
                Question.DIFFICULTY_EASY, Category.GEOGRAFIJA);
        addQuestion(q32);
        Question q33 = new Question("Koji je glavni grad Kube?",
                "Havana", "New York", "Porto", 1,
                Question.DIFFICULTY_EASY, Category.GEOGRAFIJA);
        addQuestion(q33);
        Question q34 = new Question("Što od navedenog nije kontinent?",
                "Europa", "SAD", "Sjeverna Amerika", 2,
                Question.DIFFICULTY_EASY, Category.GEOGRAFIJA);
        addQuestion(q34);
        Question q35 = new Question("Koja je najveća rijeka na svijetu?",
                "Nil", "Amazona", "Kongo", 1,
                Question.DIFFICULTY_EASY, Category.GEOGRAFIJA);
        addQuestion(q35);
        Question q36 = new Question("Najviši vrh svijeta?",
                "Himalaje", "Mount Everest", "Pirineji", 2,
                Question.DIFFICULTY_MEDIUM, Category.GEOGRAFIJA);
        addQuestion(q36);
        Question q37 = new Question("Kako se zove planina koja dijeli Aziju od Europe?",
                "Apenini", "Atlas", "Ural", 3,
                Question.DIFFICULTY_MEDIUM, Category.GEOGRAFIJA);
        addQuestion(q37);
        Question q38 = new Question("Koja je najveća pustinja na svijetu?",
                "Gobi", "Atacama", "Sahara", 3,
                Question.DIFFICULTY_MEDIUM, Category.GEOGRAFIJA);
        addQuestion(q38);
        Question q39 = new Question("Koji je najveći planet Sunčevog sustava?",
                "Mars", "Saturn", "Jupiter", 3,
                Question.DIFFICULTY_MEDIUM, Category.GEOGRAFIJA);
        addQuestion(q39);
        Question q40 = new Question("Koji je glavni grad Velike Britanije?",
                "London", "Manchester", "Sidney", 1,
                Question.DIFFICULTY_MEDIUM, Category.GEOGRAFIJA);
        addQuestion(q40);
        Question q41 = new Question("Las Vegas je?",
                "Grad kocke", "Grad droge", "Grad svijetla", 1,
                Question.DIFFICULTY_HARD, Category.GEOGRAFIJA);
        addQuestion(q41);
        Question q42 = new Question("Gdje se nalazi Sidney?",
                "Australija", "Engleska", "Slovenija", 1,
                Question.DIFFICULTY_HARD, Category.GEOGRAFIJA);
        addQuestion(q42);
        Question q43 = new Question("Gdje se bilježe najviše temperature u Sunčevom sustavu?",
                "Zemlja", "Mars", "Venera", 3,
                Question.DIFFICULTY_HARD, Category.GEOGRAFIJA);
        addQuestion(q43);
        Question q44 = new Question("Koja je najveća država svijeta?",
                "Kina", "Rusija", "Kanada", 2,
                Question.DIFFICULTY_HARD, Category.GEOGRAFIJA);
        addQuestion(q44);
        Question q45 = new Question("Odakle je Marko Polo krenuo na putovanje?",
                "Rijeka", "Venecija", "Trst", 2,
                Question.DIFFICULTY_HARD, Category.GEOGRAFIJA);
        addQuestion(q45);



    }


    private void addQuestion(Question question) {
        ContentValues cv = new ContentValues();
        cv.put(QuestionsTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuestionsTable.COLUMN_OPTION1, question.getOption1());
        cv.put(QuestionsTable.COLUMN_OPTION2, question.getOption2());
        cv.put(QuestionsTable.COLUMN_OPTION3, question.getOption3());
        cv.put(QuestionsTable.COLUMN_ANSWER_NR, question.getAnswerNr());
        cv.put(QuestionsTable.COLUMN_DIFFICULTY, question.getDifficulty());
        cv.put(QuestionsTable.COLUMN_CATEGORY_ID, question.getCategoryID());
        db.insert(QuestionsTable.TABLE_NAME, null, cv);
    }

    public List<Category> getAllCategories() {
        List<Category> categoryList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + CategoriesTable.TABLE_NAME, null);
        if (c.moveToFirst()) {
            do {
                Category category = new Category();
                category.setId(c.getInt(c.getColumnIndex(CategoriesTable._ID)));
                category.setName(c.getString(c.getColumnIndex(CategoriesTable.COLUMN_NAME)));
                categoryList.add(category);
            } while (c.moveToNext());
        }
        c.close();
        return categoryList;
    }

    public ArrayList<Question> getAllQuestions() {
        ArrayList<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + QuestionsTable.TABLE_NAME, null);
        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setId(c.getInt(c.getColumnIndex(QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION3)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_ANSWER_NR)));
                question.setDifficulty(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }
        c.close();
        return questionList;
    }

    public ArrayList<Question> getQuestions(int categoryID, String difficulty) {
        ArrayList<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        String selection = QuestionsTable.COLUMN_CATEGORY_ID + " = ? " +
                " AND " + QuestionsTable.COLUMN_DIFFICULTY + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(categoryID), difficulty};
        Cursor c = db.query(
                QuestionsTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setId(c.getInt(c.getColumnIndex(QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION3)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_ANSWER_NR)));
                question.setDifficulty(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }
        c.close();
        return questionList;
    }
}